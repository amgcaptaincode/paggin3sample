package com.amg.paggin3sample.datasource

import androidx.paging.PagingSource
import com.amg.paggin3sample.api.MyApi
import com.amg.paggin3sample.models.Passenger

class PassengersDataSource(private val api: MyApi) : PagingSource<Int, Passenger>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Passenger> {
        return try {
            val nextPage = params.key ?: 3
            val response = api.getPassengersData(nextPage)
            LoadResult.Page(
                data = response.data,
                prevKey = if (nextPage > 3) nextPage - 1 else null,
                nextKey = if (nextPage < response.totalPages) nextPage + 1 else null
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}