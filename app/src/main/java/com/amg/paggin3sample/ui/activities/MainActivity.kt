package com.amg.paggin3sample.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.amg.paggin3sample.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}