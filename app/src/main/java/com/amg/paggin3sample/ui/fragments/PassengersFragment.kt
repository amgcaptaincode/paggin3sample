package com.amg.paggin3sample.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.amg.paggin3sample.api.MyApi
import com.amg.paggin3sample.databinding.FragmentPassengersBinding
import com.amg.paggin3sample.ui.adapter.PassengersAdapter
import com.amg.paggin3sample.ui.adapter.PassengersLoadStateAdapter
import com.amg.paggin3sample.viewmodel.PassengerViewModel
import com.amg.paggin3sample.viewmodel.PassengersViewModelFactory
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class PassengersFragment : Fragment() {

    private lateinit var viewModel: PassengerViewModel
    private lateinit var binding: FragmentPassengersBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentPassengersBinding.inflate(inflater, container, false).also {
        binding = it
    }.root

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val factory = PassengersViewModelFactory(MyApi())
        viewModel = ViewModelProvider(this, factory).get(PassengerViewModel::class.java)

        val passengersAdapter = PassengersAdapter()
        binding.rvPassengers.layoutManager = LinearLayoutManager(requireContext())
        binding.rvPassengers.setHasFixedSize(true)

        binding.rvPassengers.adapter = passengersAdapter.withLoadStateHeaderAndFooter(
            header = PassengersLoadStateAdapter { passengersAdapter.retry() },
            footer = PassengersLoadStateAdapter { passengersAdapter.retry() }
        )

        lifecycleScope.launch {
            viewModel.passengers.collectLatest {
                passengersAdapter.submitData(it)
            }
        }

    }

}