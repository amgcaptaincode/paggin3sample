package com.amg.paggin3sample.models

data class Passenger(
    val __v: Int,
    val _id: String,
    val airline: Airline,
    val name: String,
    val trips: Int
)