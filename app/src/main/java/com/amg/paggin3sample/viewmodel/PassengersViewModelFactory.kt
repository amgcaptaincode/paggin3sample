package com.amg.paggin3sample.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.amg.paggin3sample.api.MyApi

@Suppress("UNCHECKED_CAST")
class PassengersViewModelFactory(private val api: MyApi) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PassengerViewModel(api) as T
    }

}