package com.amg.paggin3sample.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.amg.paggin3sample.api.MyApi
import com.amg.paggin3sample.datasource.PassengersDataSource

class PassengerViewModel(private val api: MyApi) : ViewModel() {

    val passengers = Pager(PagingConfig(pageSize = 10)) {
        PassengersDataSource(api)
    }.flow.cachedIn(viewModelScope)

}